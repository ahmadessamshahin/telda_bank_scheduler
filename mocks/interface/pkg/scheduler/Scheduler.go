// Code generated by mockery v2.9.4. DO NOT EDIT.

package mocks

import (
	scheduler "telda_bank_scheduler/interface/pkg/scheduler"

	mock "github.com/stretchr/testify/mock"
)

// Scheduler is an autogenerated mock type for the Scheduler type
type Scheduler struct {
	mock.Mock
}

// Bind provides a mock function with given fields: _a0
func (_m *Scheduler) Bind(_a0 scheduler.Cron) scheduler.Scheduler {
	ret := _m.Called(_a0)

	var r0 scheduler.Scheduler
	if rf, ok := ret.Get(0).(func(scheduler.Cron) scheduler.Scheduler); ok {
		r0 = rf(_a0)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(scheduler.Scheduler)
		}
	}

	return r0
}

// BindJobToCron provides a mock function with given fields: _a0, _a1
func (_m *Scheduler) BindJobToCron(_a0 string, _a1 scheduler.Job) error {
	ret := _m.Called(_a0, _a1)

	var r0 error
	if rf, ok := ret.Get(0).(func(string, scheduler.Job) error); ok {
		r0 = rf(_a0, _a1)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// CronInfo provides a mock function with given fields: _a0
func (_m *Scheduler) CronInfo(_a0 string) ([]byte, error) {
	ret := _m.Called(_a0)

	var r0 []byte
	if rf, ok := ret.Get(0).(func(string) []byte); ok {
		r0 = rf(_a0)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]byte)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(string) error); ok {
		r1 = rf(_a0)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetCron provides a mock function with given fields: _a0
func (_m *Scheduler) GetCron(_a0 string) (scheduler.Cron, error) {
	ret := _m.Called(_a0)

	var r0 scheduler.Cron
	if rf, ok := ret.Get(0).(func(string) scheduler.Cron); ok {
		r0 = rf(_a0)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(scheduler.Cron)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(string) error); ok {
		r1 = rf(_a0)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// RunCron provides a mock function with given fields: _a0
func (_m *Scheduler) RunCron(_a0 string) error {
	ret := _m.Called(_a0)

	var r0 error
	if rf, ok := ret.Get(0).(func(string) error); ok {
		r0 = rf(_a0)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// RunPendingCron provides a mock function with given fields:
func (_m *Scheduler) RunPendingCron() {
	_m.Called()
}

// StopCron provides a mock function with given fields: _a0
func (_m *Scheduler) StopCron(_a0 string) error {
	ret := _m.Called(_a0)

	var r0 error
	if rf, ok := ret.Get(0).(func(string) error); ok {
		r0 = rf(_a0)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}
