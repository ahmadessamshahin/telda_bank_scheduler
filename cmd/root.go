/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package cmd

import (
	"os"

	"github.com/spf13/cobra"
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "telda_bank_scheduler",
	Short: "simple scheduler",
	Long: `
		Simple scheduler  like ( Airflow )
		
		constis of  muliple cron where each cron can handle mulitple jobs 
		
		cron jobs can be updated while the cron is running 
		so in the next cron run the jobs will be executed will be updated
		
		with Two assumption:
		
		 one: 
			concern: what should happen if the job is taking time greater than 
					the cron repition time
			solution: add the new cron to waiting state till the current running corn 
					  jobs is compeleted then make the waiting cron to running state
		 
		 two: 
			concern: on shutdown the cron what should happen if the cron job is running 
					 
			solution: make graceful shut down
`,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	// Run: func(cmd *cobra.Command, args []string) { },
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	// rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.telda_bank_scheduler.yaml)")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
