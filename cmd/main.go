package cmd

import (
	"fmt"
	scheduler2 "telda_bank_scheduler/pkg/scheduler"

	"telda_bank_scheduler/pkg/scheduler/cron"
	"telda_bank_scheduler/pkg/scheduler/job"
	"time"
)

func main() {
	setupScheduler2()

}

func setupScheduler1() {
	// with jobs time < cron repetition time
	jobConfig := scheduler2.Config{
		WaitTime: 1 * time.Second,
	}
	j := job.NewSuccess(jobConfig, "SA")
	j1 := job.NewSuccess(jobConfig, "SB")
	j2 := job.NewSuccess(jobConfig, "SC")

	f := job.NewFail(jobConfig, "FA")
	f1 := job.NewFail(jobConfig, "FB")
	f2 := job.NewFail(jobConfig, "FC")

	cronConfig := scheduler2.Config{
		WaitTime: 5 * time.Second,
	}

	c := cron.NewCron(cronConfig, "CA")
	c.Bind(j).Bind(j1).Bind(f1).Bind(f2)

	s := scheduler2.NewScheduler()
	s.Bind(c)

	s.RunPendingCron()

	time.Sleep(20 * time.Second)

	// add new job to the cron c
	c.Bind(f).Bind(j2)

	// print recent history
	hist, _ := s.CronInfo(c.GetName())
	fmt.Println("History: ", string(hist))

	time.Sleep(15 * time.Second)

	c.Terminate()

	time.Sleep(10 * time.Second)

	hist, _ = s.CronInfo(c.GetName())
	fmt.Println("History: ", string(hist))
}

func setupScheduler2() {
	// with jobs time > cron repetition time
	jobConfig := scheduler2.Config{
		WaitTime: 5 * time.Second,
	}
	j := job.NewSuccess(jobConfig, "SA")

	cronConfig := scheduler2.Config{
		WaitTime: 3 * time.Second,
	}

	c := cron.NewCron(cronConfig, "CA")
	c.Bind(j)

	s := scheduler2.NewScheduler()
	s.Bind(c)

	s.RunPendingCron()

	time.Sleep(10 * time.Second)

	// print recent history
	hist, _ := s.CronInfo(c.GetName())
	fmt.Println("History: ", string(hist))

	time.Sleep(5 * time.Second)

	c.Terminate()

	time.Sleep(1 * time.Second)

	hist, _ = s.CronInfo(c.GetName())
	fmt.Println("History: ", string(hist))
}
