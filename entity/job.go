package entity

import (
	log "github.com/sirupsen/logrus"
	"sync"
	"telda_bank_scheduler/interface/pkg/scheduler"
	"time"
)

// Job response
const (
	PendingJob   scheduler.JobStatus = "pending"
	RunningJob   scheduler.JobStatus = "running"
	CompletedJob scheduler.JobStatus = "completed"
)

type Job struct {
	Name string

	WaitTime time.Duration
	status   scheduler.JobStatus

	mu sync.Mutex

	StartedAt time.Time
	StoppedAt time.Time
}

func (j Job) GetName() string {
	return j.Name
}

func (j *Job) OnStop() {

	j.StoppedAt = time.Now()
	j.SetStatus(CompletedJob)
}

func (j *Job) OnStart() {

	j.StartedAt = time.Now()
	j.SetStatus(RunningJob)
}

func (j *Job) SetStatus(s scheduler.JobStatus) {
	j.mu.Lock()
	defer j.mu.Unlock()
	j.status = s
}

func (j *Job) Status() scheduler.JobStatus {
	j.mu.Lock()
	defer j.mu.Unlock()
	log.Infof("job: %s, response: %s, at: %v  ", j.GetName(), j.status, time.Now())

	return j.status
}
