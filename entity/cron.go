package entity

import (
	"sync"
	"telda_bank_scheduler/interface/pkg/scheduler"
	"time"
)

const (
	StoppingCron scheduler.CronStatus = "Stopping"
	StoppedCron  scheduler.CronStatus = "Stopped"

	PendingCron scheduler.CronStatus = "pending"
	RunningCron scheduler.CronStatus = "running"
)

type Cron struct {
	Name string

	PendingJobs map[string]scheduler.Job
	ActiveJobs  map[string]scheduler.Job
	CreatedAt   time.Time
	UpdatedAt   time.Time
	StoppedAt   time.Time

	mu     sync.Mutex
	status scheduler.CronStatus
}

func (c *Cron) Create(name string) {
	c.ActiveJobs = make(map[string]scheduler.Job)
	c.PendingJobs = make(map[string]scheduler.Job)
	c.Name = name
	c.CreatedAt = time.Now()
	c.UpdatedAt = time.Now()
	c.SetStatus(PendingCron)
}
func (c Cron) GetName() string {
	return c.Name
}
func (c *Cron) OnUpdate() {
	c.UpdatedAt = time.Now()
}

func (c *Cron) OnStart() {
	c.SetStatus(RunningCron)
}

func (c *Cron) OnStop() {
	c.SetStatus(StoppedCron)
	c.StoppedAt = time.Now()
}

func (c *Cron) SetStatus(s scheduler.CronStatus) {
	c.mu.Lock()
	defer c.mu.Unlock()

	c.status = s
}

func (c *Cron) Status() scheduler.CronStatus {
	c.mu.Lock()
	defer c.mu.Unlock()
	return c.status
}
