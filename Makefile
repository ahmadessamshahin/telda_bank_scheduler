.MAIN: build

mock:
	mockery --all --recursive --keeptree

format:
	go fmt ./...

update:
	go mod tidy

test:
	go test ./...

build:
	go build -race --o bin/main cmd/main.go

one:
	go run main.go caseOne

two:
	go run main.go caseTwo
