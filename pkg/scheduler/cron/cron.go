package cron

import (
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"sync"
	"telda_bank_scheduler/entity"
	"telda_bank_scheduler/interface/pkg/scheduler"
	scheduler2 "telda_bank_scheduler/pkg/scheduler"
	"telda_bank_scheduler/pkg/scheduler/response"

	"time"
)

const (
	defaultScheduleTime = 5 * time.Second
)

type BaseCron struct {
	entity.Cron

	// sync shared memory (entity cron)
	mu sync.Mutex

	// notify cron with job response
	finishSignal chan response.JobStatus

	// Stop running cron
	quit chan bool

	//	run every
	every time.Duration

	//	logging
	Logger log.Logger

	//	cron history
	History map[time.Time][]response.JobStatus

	//	conditional lock to block running new cron if the old still running
	cronCond *sync.Cond
}

func NewCron(config scheduler2.Config, name string) scheduler.Cron {
	c := &BaseCron{}

	c.every = defaultScheduleTime
	if int(config.WaitTime) != 0 {
		c.every = config.WaitTime
	}

	c.Create(name)

	c.quit = make(chan bool)

	mu := sync.Mutex{}
	c.cronCond = sync.NewCond(&mu)

	c.History = make(map[time.Time][]response.JobStatus)
	return c

}

// Bind new job to the bending list
// will be executed next time cron run

func (c *BaseCron) Bind(job scheduler.Job) scheduler.Cron {
	c.PendingJobs[job.GetName()] = job
	return c
}

// Cron runner

func (c BaseCron) Run() {
	heartBeat := func() {
		defer c.OnStop()
		ticker := time.NewTicker(c.every)
		for {
			select {
			case t := <-ticker.C:
				err := c.load()

				// Assumption: cron interval < job execution interval
				// wait (cron interval) between every job execution
				if err != nil {
					log.Warnf("jobs execution time > cron repetition interval with error: %v ", err)
					ticker.Stop()
					c.waitForChannelsToClose()
					_ = c.load()
					ticker.Reset(c.every)
				}

				// cron history setup
				c.History[t] = make([]response.JobStatus, 0)

				go c.fireActiveJobsWorker(t)

			case <-c.quit:
				ticker.Stop()
				log.Error("cron terminating")
				// graceful stop wait till the running jobs to be completed
				if c.isAnyJobRunning() {
					c.waitForChannelsToClose()
				}
				log.Error("cron terminated")

				c.OnStop()
				return
			}
		}
	}

	c.OnStart()

	//c.historyWorker()

	go heartBeat()
}
func (c BaseCron) Info() []byte {
	json, err := json.Marshal(c.History)
	if err != nil {
		log.Errorf("failed to marshal cron history error %v", err)
	}
	return json
}

func (c BaseCron) Terminate() {
	c.SetStatus(entity.StoppingCron)
	c.quit <- true
}

// append the pending jobs to active jobs

func (c *BaseCron) load() error {
	// With Assumption:  Overwrite the existing one
	if !c.isAnyJobRunning() {
		c.updateActiveJobs()
		return nil
	}
	return fmt.Errorf("can't load pending jobs since the there still job running for cron: %s", c.Name)
}

// initialize finish signals
// fire side worker to update cron history
// adjust job config and run it

func (c BaseCron) fireActiveJobsWorker(t time.Time) {
	c.mu.Lock()
	defer c.mu.Unlock()

	c.initializeFinishSignals(len(c.ActiveJobs))

	c.fireActiveJob(t)

	close(c.finishSignal)

	c.updateCronHistory(t)

	c.cronCond.Signal()
}

func (c BaseCron) fireActiveJob(t time.Time) {
	i := 0
	wg := sync.WaitGroup{}

	log.Infof("cron %s, started at: %v", c.Name, time.Now())

	for _, j := range c.ActiveJobs {
		wg.Add(1)
		j.Config(c.finishSignal, t)
		j.Run(&wg)
		i++
	}
	log.Infof("cron %s, completed at: %v", c.Name, time.Now())

	wg.Wait()

}

// finish single worker to update the cron history @ given time with its job output

func (c BaseCron) updateCronHistory(t time.Time) {
	for m := range c.finishSignal {
		c.History[t] = append(c.History[t], m)
	}
	log.Infof("update cron %s at time stamp %v history", c.GetName(), t)
}

// create channels based on the new active jobs

func (c *BaseCron) initializeFinishSignals(i int) {
	c.finishSignal = make(chan response.JobStatus, i)
}

// check for running jobs

func (c BaseCron) isAnyJobRunning() bool {
	c.mu.Lock()
	defer c.mu.Unlock()
	for _, j := range c.ActiveJobs {
		if j.Status() == entity.RunningJob {
			return true
		}
	}
	return false
}

// modify the active jobs

func (c BaseCron) updateActiveJobs() {
	c.mu.Lock()
	defer c.mu.Unlock()
	for k, v := range c.PendingJobs {
		c.ActiveJobs[k] = v
		delete(c.PendingJobs, k)
	}
}

// waiting on the jobs to be finishSignal

func (c BaseCron) waitForChannelsToClose() {
	c.cronCond.L.Lock()
	defer func() {
		c.cronCond.L.Unlock()
		log.Warn("return cron back to work")
	}()

	log.Warn("wait old cron to be completed")

	c.cronCond.Wait()
}
