package cron

import (
	"telda_bank_scheduler/interface/pkg/scheduler"
	mocks "telda_bank_scheduler/mocks/interface/pkg/scheduler"
	scheduler2 "telda_bank_scheduler/pkg/scheduler"
	"time"
)

func setupCron() (scheduler.Cron, *mocks.Job) {

	jobMock := new(mocks.Job)
	c := scheduler2.Config{
		WaitTime: 2 * time.Second,
	}

	cron := NewCron(c, "CA")
	cron.Bind(jobMock)

	return cron, jobMock
}


