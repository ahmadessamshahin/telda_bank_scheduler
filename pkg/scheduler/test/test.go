package test

import "testing"

type SchedulerTestingTable struct {
	Name             string
	MockingFunc      func()
	Validation       []func(...interface{}) bool
	ServiceBuildFunc func() []interface{}
}

func TestingEntryIterator(t *testing.T, cases []SchedulerTestingTable) {
	for _, c := range cases {
		t.Run(c.Name, func(t *testing.T) {
			c.MockingFunc()
			if c.ServiceBuildFunc == nil {
				t.Fatal("MISSING RequestBuildFunc")
			}
			res := c.ServiceBuildFunc()
			Exec(t, res, c.Validation...)
		})
	}
}

func Exec(t *testing.T, res []interface{}, fs ...func(...interface{}) bool) {
	for _, f := range fs {
		if !f(res...) {
			t.Fail()
		}
	}
}
