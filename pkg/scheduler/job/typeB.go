package job

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"sync"
	"telda_bank_scheduler/interface/pkg/scheduler"
	schedular2 "telda_bank_scheduler/pkg/scheduler"
	"time"
)

type Fail struct {
	BaseJob
}

func NewFail(c schedular2.Config, name string) scheduler.Job {
	j := &Fail{}

	j.WaitTime = schedular2.DefaultDuration
	if int(c.WaitTime) != 0 {
		j.WaitTime = c.WaitTime
	}

	j.Create(name)

	return j
}

func (j *Fail) Run(wg *sync.WaitGroup) error {
	if err := j.isFinishSignalExist(); err != nil {
		log.Errorf("the finsih signal doesn't exist %v", err)
		wg.Done()
		return err
	}

	j.BaseJob.Run(wg,
		func() error {
			time.Sleep(j.WaitTime)
			return fmt.Errorf("error while job running")
		})

	return nil
}
