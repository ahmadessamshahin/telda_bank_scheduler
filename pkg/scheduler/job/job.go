package job

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"sync"
	"telda_bank_scheduler/entity"
	"telda_bank_scheduler/interface/pkg/scheduler"
	"telda_bank_scheduler/pkg/scheduler/response"
	"time"
)

type BaseJob struct {
	entity.Job

	//notify the cron on complete
	finishSignal chan<- response.JobStatus

	//logging
	Logger log.Logger

	//	cron repetition start time

	RepetitionStartTime time.Time
}

func (j *BaseJob) Create(name string) {
	j.Name = name
	j.SetStatus(entity.PendingJob)
}

func (j *BaseJob) Run(wg *sync.WaitGroup, f scheduler.JobFunc) {
	j.OnStart()
	go func() {
		defer func() {
			j.OnStop()
			wg.Done()
		}()
		err := f()
		j.finishSignal <- response.JobStatus{Name: j.Name, Message: err, CronStartTime: j.RepetitionStartTime}

	}()
}

func (j *BaseJob) Config(c chan<- response.JobStatus, time time.Time) {
	j.finishSignal = c
	j.RepetitionStartTime = time
}

func (j BaseJob) isFinishSignalExist() error {
	if j.finishSignal == nil {
		return fmt.Errorf("finish signal is not exist for job %s", j.GetName())
	}
	return nil
}
