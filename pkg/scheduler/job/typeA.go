package job

import (
	log "github.com/sirupsen/logrus"
	"sync"
	"telda_bank_scheduler/interface/pkg/scheduler"
	schedular2 "telda_bank_scheduler/pkg/scheduler"
	"time"
)

type Success struct {
	BaseJob
}

func NewSuccess(c schedular2.Config, name string) scheduler.Job {
	j := &Success{}

	j.WaitTime = schedular2.DefaultDuration
	if int(c.WaitTime) != 0 {
		j.WaitTime = c.WaitTime
	}

	j.Create(name)

	return j
}

func (j *Success) Run(wg *sync.WaitGroup) error {
	if err := j.isFinishSignalExist(); err != nil {
		log.Errorf("the finsih signal doesn't exist %v", err)
		wg.Done()
		return err
	}

	j.BaseJob.Run(wg,
		func() error {
			j.Logger.Infof("start job %v", time.Now())

			time.Sleep(j.WaitTime)
			j.Logger.Infof("end job %v", time.Now())

			return nil
		})

	return nil
}
