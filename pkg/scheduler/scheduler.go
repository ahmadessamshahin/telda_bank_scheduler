package scheduler

import (
	"fmt"
	log "github.com/sirupsen/logrus"

	"sync"
	"telda_bank_scheduler/entity"
	"telda_bank_scheduler/interface/pkg/scheduler"
)

var (
	once     sync.Once
	instance *Scheduler
)

type Scheduler struct {
	Cron   map[string]scheduler.Cron
	Logger log.Logger
}

func NewScheduler() scheduler.Scheduler {

	once.Do(func() {
		s := &Scheduler{}
		s.Cron = make(map[string]scheduler.Cron)
		instance = s
	})

	return instance
}

func (s Scheduler) Bind(cron scheduler.Cron) scheduler.Scheduler {
	s.Cron[cron.GetName()] = cron
	return s
}

func (s Scheduler) GetCron(name string) (scheduler.Cron, error) {
	if v, ok := s.Cron[name]; ok {
		return v, nil
	}
	return nil, fmt.Errorf("the cron with this name %s not exist", name)
}

func (s Scheduler) BindJobToCron(name string, job scheduler.Job) error {
	cron, err := s.GetCron(name)

	if err != nil {
		return err
	}
	cron.Bind(job)
	return nil
}

func (s Scheduler) RunPendingCron() {
	for _, c := range s.Cron {
		if c.Status() == entity.PendingCron {
			c.Run()
		}
	}
}

func (s Scheduler) StopCron(name string) error {
	cron, err := s.GetCron(name)

	if err != nil {
		return err
	}

	if cron.Status() != entity.RunningCron {
		return fmt.Errorf("this cron is not running")
	}

	cron.Terminate()
	return nil
}

func (s Scheduler) RunCron(name string) error {
	cron, err := s.GetCron(name)

	if err != nil {
		return err
	}

	if cron.Status() == entity.RunningCron {
		return fmt.Errorf("this cron is running")
	}

	cron.Run()
	return nil
}

func (s Scheduler) CronInfo(name string) ([]byte, error) {
	cron, err := s.GetCron(name)

	if err != nil {
		return nil, err
	}

	return cron.Info(), nil
}
