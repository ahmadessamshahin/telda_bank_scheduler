package scheduler

import (
	"time"
)

const (
	DefaultDuration = 2 * time.Second
)

type Config struct {
	WaitTime time.Duration
}
