package response

import "time"

type JobStatus struct {
	Name          string
	CronStartTime time.Time
	Message       error
}
