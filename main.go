/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package main

import "telda_bank_scheduler/cmd"

func main() {
	cmd.Execute()
}
