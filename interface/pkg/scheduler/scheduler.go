package scheduler

type Scheduler interface {
	Bind(Cron) Scheduler
	GetCron(string) (Cron, error)
	BindJobToCron(string, Job) error
	RunPendingCron()
	StopCron(string) error
	RunCron(string) error
	CronInfo(string) ([]byte, error)
}
