package scheduler

import (
	"sync"
	"telda_bank_scheduler/pkg/scheduler/response"
	"time"
)

type JobStatus string

type JobFunc func() error

type Job interface {
	GetName() string
	Run(*sync.WaitGroup) error
	Config(chan<- response.JobStatus, time.Time)
	Status() JobStatus
}
