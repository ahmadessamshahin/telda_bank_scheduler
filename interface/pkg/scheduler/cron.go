package scheduler

type CronStatus string

type Cron interface {
	GetName() string
	Bind(Job) Cron
	Run()
	Terminate()
	Status() CronStatus
	Info() []byte
}
